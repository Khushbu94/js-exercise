/*
 *  Write a JavaScript program to validate a date in dd/mm/yyyy format. If the user input matches with
 *  the format, the program will return a message "Valid Date" otherwise return a message "Invalid Date!"
 */

function checkDate(date){
  var reg = /(0[1-9]|[12][0-9]|3[01])[/.](0[1-9]|1[012])[/.](19|20)\d\d/;
  if (date.match(reg)) {
    return "Valid Date";
  } else {
    return "Invalid Date!";
  }
}