/*
 *  Design a cash register drawer function that accepts purchase price as the first argument, payment as
 *  the second argument, and cash-in-drawer (cid) as the third argument.
 *
 *  cid is a 2d array listing available currency.
 *
 *  Return the string "Insufficient Funds" if cash-in-drawer is less than the change due. Return the
 *  string "Closed" if cash-in-drawer is equal to the change due.
 *
 *  Otherwise, return change in coin and bills, sorted in highest to lowest order.
 *  In case of multiple solutions, return the one containing the highest value of coin/bill
 */

var cid = [
  ['ONE RUPEE', 6],
  ['TWO RUPEES', 7],
  ['FIVE RUPEES', 5],
  ['TEN RUPEES', 1],
  ['TWENTY RUPEES', 2],
  ['ONE HUNDRED RUPEES', 2],
  ['ONE THOUSAND RUPEES', 1]
];

function drawer(price, cash, cid) {
  var change = cash - price;
  totalCid = 0;
  for (var i = 0; i < cid.length; i++) {
    if (cid[i][0] === 'ONE RUPEE') {

      totalCid += cid[i][1] * 1;

    } else if (cid[i][0] === 'TWO RUPEES') {

      totalCid += cid[i][1] * 2;

    } else if (cid[i][0] === 'FIVE RUPEES') {

      totalCid += cid[i][1] * 5;

    } else if (cid[i][0] === 'TEN RUPEES') {

      totalCid += cid[i][1] * 10;

    } else if (cid[i][0] === 'TWENTY RUPEES') {

      totalCid += cid[i][1] * 20;

    } else if (cid[i][0] === 'ONE HUNDRED RUPEES') {

      totalCid += cid[i][1] * 100;

    } else if (cid[i][0] === 'ONE THOUSAND RUPEES') {

      totalCid += cid[i][1] * 1000;

    }
  }
  if (totalCid < change) {
    return "Insufficient Funds";
  } else if (totalCid == change) {
    return "Closed";
  }
  var changeGiven = [
    ['ONE RUPEE', 0],
    ['TWO RUPEES', 0],
    ['FIVE RUPEES', 0],
    ['TEN RUPEES', 0],
    ['TWENTY RUPEES', 0],
    ['ONE HUNDRED RUPEES', 0],
    ['ONE THOUSAND RUPEES', 0]
  ];
  changeLeft = change;
  while (changeLeft > 0) {
    if (changeLeft >= 1000 && cid[6][1] > 0) {
      changeLeft -= 1000;
      changeGiven[6][1] += 1;
    } else if (changeLeft >= 100 && cid[5][1] > 0) {
      changeLeft -= 100;
      changeGiven[5][1] += 1;
    } else if (changeLeft >= 20 && cid[4][1] > 0) {
      changeLeft -= 20;
      changeGiven[4][1] += 1;
    } else if (changeLeft >= 10 && cid[3][1] > 0) {
      changeLeft -= 10;
      changeGiven[3][1] += 1;
    } else if (changeLeft >= 5 && cid[2][1] > 0) {
      changeLeft -= 5;
      changeGiven[2][1] += 1;
    } else if (changeLeft >= 2 && cid[1][1] > 0) {
      changeLeft -= 2;
      changeGiven[1][1] += 1;
    } else if (changeLeft >= 1 && cid[0][1] > 0) {
      changeLeft -= 1;
      changeGiven[0][1] += 1;
    }
  }

  for (var j = 0; j < changeGiven.length; j++) {
    if (changeGiven[j][1] <= 0) {
      changeGiven.splice(j, 1);
      j = -1;
    }
  }
  // Here is your change, ma'am.
  return changeGiven;
}
