/*
 * Write a function findLongestWord() that takes an array of words and returns the length of the longest word.
 *
 */


function findLongestWord(words) {
  var longestWord = '';
  for (var i = 0; i < words.length; i++) {
    if (words[i].length > longestWord.length) {
      longestWord = words[i];
    }
  }
  return longestWord.length;
}
