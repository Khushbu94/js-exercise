/**
 * Given two input strings, write a function to find largest identical substring
 * For example:
 * String
 * Spring
 * Here the largest substring is 'ring'
 *
 * In case there are multiple largest substrings of similar length, the function must return the first largest substring found w.r.t first string, For example:
 * hirespring
 * ringfhire
 * Here, the largest substring returned will be 'hire' and NOT 'ring'
 */






function largestSubstring(str1, str2) {
  var testSubstring = "";
  strr1 = "";
  strr2 = "";
  strr3 = "";
  for (var i = 0; i < str1.length; i++) {
    testSubstring1 = str1.substring(0, str1.length - i);
    if (str2.indexOf(testSubstring1) !== -1) {
      if (strr1 === "") {
        strr1 = testSubstring1;
      }
    }
    testSubstring2 = str1.substring(i, str1.length);
    if (str2.indexOf(testSubstring2) !== -1) {
      if (strr2 === "") {

        strr2 = testSubstring2;
      }
    }
    testSubstring3 = str1.substring(i, str1.length - i);
    if (str2.indexOf(testSubstring3) !== -1) {
      if (strr3 === "") {
        strr3 = testSubstring3;
      }
    }
  }

  if (strr1.length >= strr2.length) {
    if (strr1.length >= strr3.length) {
      return strr1;
    } else {
      return strr3;
    }
  } else {
    if (strr2.length >= strr3.length) {
      return strr2;
    } else {
      return strr3;
    }
  }

  return "";
}
