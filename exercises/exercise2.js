/*
 *  Write a function filterLongWords() that takes an array of words and an integer i and returns the array
 *  of words that are longer than i.
 */

function filterLongWords(words, i) {
  var longWords = [];

  for (var j = 0; j < words.length; j++) {

    if (words[j].length > i) {

      longWords.push(words[j]);
    }

  }
  return longWords;
}
