/*
 *  Define a function reverse() that computes the reversal of a (full) string. For example,
 *  reverse("jag testar") should return the string "ratset gaj".
 */


function reverse(str) {
  var newString = "";
  for (var i = str.length - 1; i >= 0; i--) {
    newString += str[i];
  }
  return newString;
}